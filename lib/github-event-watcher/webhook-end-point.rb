# Copyright (C) 2023  Daijiro Fukuda <fukuda@clear-code.com>
# Copyright (C) 2023  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module GitHubEventWatcher
  class WebhookEndPoint
    attr_reader :uri
    def initialize(uri, event_types, repositories)
      @uri = uri
      @event_types = event_types
      @repositories = repositories
    end

    def target?(event, repository)
      @event_types.include?(event.type) and @repositories.include?(repository)
    end
  end
end
